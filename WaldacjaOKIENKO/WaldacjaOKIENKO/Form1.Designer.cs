﻿namespace WaldacjaOKIENKO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.file = new System.Windows.Forms.Button();
            this.path = new System.Windows.Forms.TextBox();
            this.yesOrNo = new System.Windows.Forms.Label();
            this.checkNumber = new System.Windows.Forms.Button();
            this.textMisteake = new System.Windows.Forms.Label();
            this.numery = new System.Windows.Forms.ListBox();
            this.clean = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textNumber
            // 
            this.textNumber.Location = new System.Drawing.Point(21, 45);
            this.textNumber.Name = "textNumber";
            this.textNumber.Size = new System.Drawing.Size(203, 20);
            this.textNumber.TabIndex = 0;
            this.textNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumber_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Numer karty lub ścieżka do pliku:";
            // 
            // file
            // 
            this.file.Location = new System.Drawing.Point(230, 69);
            this.file.Name = "file";
            this.file.Size = new System.Drawing.Size(75, 23);
            this.file.TabIndex = 2;
            this.file.Text = "Z pliku";
            this.file.UseVisualStyleBackColor = true;
            this.file.Click += new System.EventHandler(this.read_Click);
            // 
            // path
            // 
            this.path.Location = new System.Drawing.Point(21, 71);
            this.path.Name = "path";
            this.path.Size = new System.Drawing.Size(203, 20);
            this.path.TabIndex = 3;
            // 
            // yesOrNo
            // 
            this.yesOrNo.AutoSize = true;
            this.yesOrNo.Location = new System.Drawing.Point(18, 193);
            this.yesOrNo.Name = "yesOrNo";
            this.yesOrNo.Size = new System.Drawing.Size(0, 13);
            this.yesOrNo.TabIndex = 4;
            // 
            // checkNumber
            // 
            this.checkNumber.Location = new System.Drawing.Point(12, 114);
            this.checkNumber.Name = "checkNumber";
            this.checkNumber.Size = new System.Drawing.Size(75, 23);
            this.checkNumber.TabIndex = 5;
            this.checkNumber.Text = "Sprawdź";
            this.checkNumber.UseVisualStyleBackColor = true;
            this.checkNumber.Click += new System.EventHandler(this.checkNumber_Click);
            // 
            // textMisteake
            // 
            this.textMisteake.AutoSize = true;
            this.textMisteake.Location = new System.Drawing.Point(231, 45);
            this.textMisteake.Name = "textMisteake";
            this.textMisteake.Size = new System.Drawing.Size(0, 13);
            this.textMisteake.TabIndex = 7;
            // 
            // numery
            // 
            this.numery.FormattingEnabled = true;
            this.numery.Location = new System.Drawing.Point(416, 32);
            this.numery.Name = "numery";
            this.numery.Size = new System.Drawing.Size(168, 251);
            this.numery.TabIndex = 9;
            // 
            // clean
            // 
            this.clean.Location = new System.Drawing.Point(230, 114);
            this.clean.Name = "clean";
            this.clean.Size = new System.Drawing.Size(75, 23);
            this.clean.TabIndex = 10;
            this.clean.Text = "Czyść";
            this.clean.UseVisualStyleBackColor = true;
            this.clean.Click += new System.EventHandler(this.clean_click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(416, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Lista numerów prawidłowych:";
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(416, 289);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(168, 23);
            this.delete.TabIndex = 12;
            this.delete.Text = "Usuń";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 331);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.clean);
            this.Controls.Add(this.numery);
            this.Controls.Add(this.textMisteake);
            this.Controls.Add(this.checkNumber);
            this.Controls.Add(this.yesOrNo);
            this.Controls.Add(this.path);
            this.Controls.Add(this.file);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textNumber);
            this.Name = "Form1";
            this.Text = "Walidacja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button file;
        private System.Windows.Forms.TextBox path;
        private System.Windows.Forms.Label yesOrNo;
        private System.Windows.Forms.Button checkNumber;
        private System.Windows.Forms.Label textMisteake;
        private System.Windows.Forms.ListBox numery;
        private System.Windows.Forms.Button clean;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button delete;
    }
}

