﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace WaldacjaOKIENKO
{
    public partial class Form1 : Form
    {
        string numberCard;
        const string fileName = "numbers.txt";

        public Form1()
        {
            InitializeComponent();
            path.ReadOnly = true;
            ShowItemsOnList();
        }

        public void ShowItemsOnList()
        {
            numery.Items.Clear();
            List<string> list = getItemsToList();
            foreach(string line in list)
            {
                numery.Items.Add(line);
            }
        }

        public List<string> getItemsToList()
        {
            List<string> fileListTemp = new List<string>();
            if (!File.Exists(fileName))
            {
                return null;
            }
            using (StreamReader sr = File.OpenText(fileName))
            {
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    fileListTemp.Add(s);
                }
            }
            return fileListTemp;
        }

        public void AddItemToList(string n)
        {
            if (numery.Items.Contains(n) == false)
            {
                TextWriter tsw = new StreamWriter(fileName, true);
                tsw.WriteLine(n);
                tsw.Close();
            }
            numery.Items.Add(textNumber.Text);
        }

        private void read_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path.Text = dialog.FileName;
                numberCard = File.ReadAllText(dialog.FileName);
                textNumber.Text = File.ReadAllText(dialog.FileName);
            }
        }
        private void checkNumber_Click(object sender, EventArgs e)
        {
            numberCard = textNumber.Text;
            if (textNumber.Text == "")
            {
                textMisteake.Text = "Nie podałeś żadnej wartości";
                yesOrNo.Text = "";
            }
            else if (textNumber.Text.Length != 16)
            {
                textMisteake.Text = "Nieodpowiednia liczba znaków";
                yesOrNo.Text = "";
            }
            else
            {
                textMisteake.Text = "";
                bool result = Walidation.CardWalidation(numberCard);
                if (result)
                {
                    yesOrNo.Text = "Numer karty jest prawidłowy";
                    
                    if(!numery.Items.Contains(textNumber.Text))
                    {
                        AddItemToList(textNumber.Text);
                        //numery.Items.Add(textNumber.Text);
                    }
                }
                else
                {
                    yesOrNo.Text = "Numer karty jest nieprawidłowy";
                }
            }
        }
        private void clean_click(object sender, EventArgs e)
        {
            yesOrNo.Text = "";
            textMisteake.Text = "";
            textNumber.Text = "";
            path.Text = "";
        }

        private void textNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            List<string> list = getItemsToList();
            list.RemoveAt(numery.SelectedIndex);
            using (StreamWriter sWriter = new StreamWriter(fileName, false))
            {
                foreach (var line in list)
                {
                    sWriter.WriteLine(line);
                }
            }
            ShowItemsOnList();
        }
    }
}
