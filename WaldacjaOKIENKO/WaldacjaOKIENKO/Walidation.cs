﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WaldacjaOKIENKO
{
    class Walidation
    {
        public static bool CardWalidation(string number)
        {
            int sum = 0;
            List<int> list = new List<int>();
            for (int i = 0; i < number.Length; i++)
            {
                int numberToInt = (int)char.GetNumericValue(number[i]);
                if (i % 2 == 0)
                {
                    numberToInt = numberToInt * 2;
                }
                list.Add(numberToInt);
            }
            string numberToString = string.Join("", list.Select(n => n.ToString()).ToArray());
            for (int i = 0; i < numberToString.Length; i++)
            {
                int numberToInt2 = (int)char.GetNumericValue(numberToString[i]);
                sum += numberToInt2;
            }
            if (sum % 10 == 0)
            {
                return true;
            }
            return false;
        }
    }
}
